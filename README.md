#mina-app-server

mina-app-server 是一个 Java 的远程调用框架，基于 
[Apache Mina](http://mina.apache.org/)。它主要用于开发独立的远程调用的服务。

目前的最新版本是 2.0.4-SNAPSHOT，另外版本 [3.0.0](http://git.oschina.net/yidinghe/mina-app-server/tree/3.0.0/) 已经开发完毕。

2017-04-11 :

* 修复了当使用 Spring 时，管理页面中可能出现无法列出接口类的问题。

2017-03-17 : V2.0.4-SNAPSHOT

* 因为 fastjson 存在漏洞，更新了依赖关系。

详细信息请参考 [wiki](http://git.oschina.net/yidinghe/mina-app-server/wikis/home)。

本项目源代码托管的唯一地址是开源中国 git@osc，如果有什么疑问或 BUG 报告，请尽管在这里的 [Issues](http://git.oschina.net/yidinghe/mina-app-server/issues) 中讨论和提交。